package com.test.applicationtmdb.Views;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Movie;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.test.applicationtmdb.Adapter.MoviesAdapter;
import com.test.applicationtmdb.R;
import com.test.applicationtmdb.Services.GetMoviesBySearch;
import com.test.applicationtmdb.Services.GetPopularMovies;
import com.test.applicationtmdb.WebService.Response.Movies;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private Movies movies = new Movies();
    private MoviesAdapter adapter;
    private CircleProgressBar progressBar;
    private EditText querySearch;
    private int currentPage = 0;
    private String currentQuery = "";
    private GetPopularMovies getPopular;
    private GetMoviesBySearch getMoviesBySearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressBar = (CircleProgressBar) findViewById(R.id.progressBar);
        progressBar.setColorSchemeResources(R.color.colorAccent);

        recycler = (RecyclerView) findViewById(R.id.recycler);
        initRecycler();
        getMoviesPerPage(1,false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.hint_search));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.equals("")){
                    Log.d("QUERY", "VACIO");
                    checkFieldAndSearch("");
                }else{
                    Log.d("QUERY", s);
                    checkFieldAndSearch(s);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.search:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRecycler(){
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL,false));
        adapter = new MoviesAdapter(this);
        recycler.setAdapter(adapter);
    }

    public void getMoviesPerPage(int page, boolean cancel){
        currentPage = page;
        currentQuery = "";
        progressBar.setVisibility(View.VISIBLE);
        if (cancel){
            if (getPopular!=null)
                if (getPopular.getCall()!=null){
                    if (getPopular.getCall().isExecuted()){
                        Log.v("CallExecuting","Canceling..");
                        getPopular.getCall().cancel();
                        hideProgress();
                    }
                }
        }
        getPopular = new GetPopularMovies();
        getPopular.getMovies(page,this);
    }

    public void getMoviesPerQuery(int page, String query, boolean cancel){
        currentPage = page;
        currentQuery = query;
        progressBar.setVisibility(View.VISIBLE);
        if (cancel){
            if (getMoviesBySearch!=null)
                if (getMoviesBySearch.getCall()!=null){
                    if (getMoviesBySearch.getCall().isExecuted()){
                        Log.v("CallExecuting","Canceling..");
                        getMoviesBySearch.getCall().cancel();
                        hideProgress();
                    }
                }
        }
        getMoviesBySearch = new GetMoviesBySearch();
        getMoviesBySearch.getMovies(page,this,query);
    }

    public void fillAdapter(Movies movies){
        if (movies != null){
            this.movies = movies;
            if (movies.getPage() == 1){
                adapter.setResult(movies.getResults());
            }else if (movies.getPage() > 1){
                adapter.addMoreResults(movies.getResults());
            }

            Log.v("MoviesApi","RESPONSE2:"+String.valueOf(movies.getTotalResults()));
            progressBar.setVisibility(View.GONE);
        }else{
            Log.v("MoviesApi","RESPONSE2:NULL");
        }
    }

    public void checkFieldAndSearch(String s){
        String query = s;
        if (query.equals("")){
            getMoviesPerPage(1,true);
        }else{
            getMoviesPerQuery(1,query,true);
        }
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getCurrentQuery() {
        return currentQuery;
    }

    public void setCurrentQuery(String currentQuery) {
        this.currentQuery = currentQuery;
    }

    public Movies getMovies() {
        return movies;
    }

    public void setMovies(Movies movies) {
        this.movies = movies;
    }

    public void hideProgress(){
        progressBar.setVisibility(View.GONE);
    }

    public void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }
}
