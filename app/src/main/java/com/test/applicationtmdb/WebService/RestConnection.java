package com.test.applicationtmdb.WebService;

import com.test.applicationtmdb.WebService.Response.Movies;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by andresrodriguez on 2/10/17.
 */
public interface RestConnection {
    @GET
    Call<Movies> getMovies(@Url String url);

}
