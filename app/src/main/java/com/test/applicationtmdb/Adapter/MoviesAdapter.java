package com.test.applicationtmdb.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.applicationtmdb.R;
import com.test.applicationtmdb.Views.MainActivity;
import com.test.applicationtmdb.WebService.Response.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresrodriguez on 2/10/17.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    private Context adapterContext;
    private List<Result> moviesList = null;
    private MainActivity activity = null;

    public List<Result> getResult() {
        return moviesList;
    }

    public void setResult(List<Result> movies) {
        this.moviesList.clear();
        this.moviesList.addAll(movies);
        notifyDataSetChanged();
    }

    public void addMoreResults(List<Result> movies){
        this.moviesList.addAll(movies);
        notifyItemInserted(getResult().size() - 1);
    }

    public static class MoviesViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;
        public TextView movieTitle;
        public TextView movieYear;
        public TextView movieOverView;

        public MoviesViewHolder(View v) {
            super(v);
            poster = (ImageView) v.findViewById(R.id.movieImage);
            movieTitle = (TextView) v.findViewById(R.id.movieTitle);
            movieOverView = (TextView) v.findViewById(R.id.movieOverview);
            movieYear = (TextView) v.findViewById(R.id.movieYear);
        }
    }

    public MoviesAdapter(Context context) {
        adapterContext = context;
        if (adapterContext instanceof MainActivity){
            activity = (MainActivity) adapterContext;
        }
        moviesList = new ArrayList<Result>();
        Log.v("ResultSize",String.valueOf(getResult().size()));

    }

    @Override
    public MoviesAdapter.MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);
        return new MoviesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MoviesAdapter.MoviesViewHolder holder, final int position) {
        if (getResult().size()>0){
            if (position == getResult().size() - 1) {
                if (activity!=null){
                    if (activity.getCurrentQuery().equals("")){
                        int nextPage = activity.getCurrentPage() + 1;
                        if (nextPage <= activity.getMovies().getTotalPages()){
                            activity.showProgress();
                            activity.getMoviesPerPage(nextPage,true);
                        }else{
                            activity.hideProgress();
                        }
                    }else{
                        int nextPage = activity.getCurrentPage() + 1;
                        String query = activity.getCurrentQuery();
                        if (nextPage <= activity.getMovies().getTotalPages()){
                            activity.showProgress();
                            activity.getMoviesPerQuery(nextPage,query,true);
                        }else{
                            activity.hideProgress();
                        }
                    }
                }
            }
            Result movie = moviesList.get(position);

            holder.movieTitle.setText(movie.getTitle());
            holder.movieYear.setText(movie.getReleaseDate());
            holder.movieOverView.setText(movie.getOverview());
            Picasso.with(adapterContext)
                    .load(movie.getPosterPath())
                    .placeholder(R.color.colorAccent)
                    .into(holder.poster);
        }
    }

    @Override
    public int getItemCount() {
        return getResult().size();
    }
}
