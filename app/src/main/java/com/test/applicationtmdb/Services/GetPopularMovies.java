package com.test.applicationtmdb.Services;

import android.content.Context;
import android.util.Log;

import com.test.applicationtmdb.Views.MainActivity;
import com.test.applicationtmdb.WebService.Response.Movies;
import com.test.applicationtmdb.WebService.RestConnection;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 2/10/17.
 */
public class GetPopularMovies {
    final private String API_KEY="93aea0c77bc168d8bbce3918cefefa45";
    private Movies movies;
    private Context mContext;
    private MainActivity activity;
    private Call<Movies> call;

    public void getMovies(int page,final Context context){
        if (page==0)
            page = 1;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Customize the request
                Request request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Authorization", "auth-token")
                        .method(original.method(), original.body())
                        .build();
                okhttp3.Response response = chain.proceed(request);
                Log.v("Response",response.toString());
                // Customize or return the response
                return response;
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        RestConnection service = retrofit.create(RestConnection.class);
        call = service.getMovies("popular?" +
                "api_key=" +
                API_KEY +
                "&page=" +
                String.valueOf(page));
        call.enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                if (response.isSuccessful()) {
                    movies = response.body();

                    if (movies != null){
                        if (context instanceof MainActivity){
                            activity = (MainActivity) context;
                            activity.fillAdapter(movies);
                            Log.v("MoviesApi","RESPONSE:SUCCESSS");
                            Log.v("MoviesApi","RESPONSE:"+String.valueOf(movies.getTotalResults()));
                        }
                    }

                } else {
                    Log.v("MoviesApi","RESPONSE:ERROR");
                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();
                    Log.v("MoviesApi",errorBody.toString());
                    movies = null;
                }
            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                Log.e("MoviesApi","onFailure: ",t);
            }
        });
    }

    public Call<Movies> getCall() {
        return call;
    }

    public void setCall(Call<Movies> call) {
        this.call = call;
    }
}
